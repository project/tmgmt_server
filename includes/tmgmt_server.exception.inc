<?php

/**
 * @file
 *
 * tmgmt_server exception classes.
 */


class TMGMTServerConnectionException extends TMGMTException {

}

class TMGMTServerValidationException extends TMGMTException {

}

/**
 * Exception class for translation order exceptions.
 */
class TMGMTServerTranslationOrderException extends ServicesException {

}
