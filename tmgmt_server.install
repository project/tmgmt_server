<?php

/**
 * @file
 * Install, update and uninstall functions for the Translation Management Server module.
 */


/**
 * Adds ds_uid field.
 * Recreates and adds indexes to the client table.
 */
function tmgmt_server_update_7001() {
  $schema = tmgmt_server_schema();

  if (!db_field_exists('tmgmt_server_client', 'ds_uid')) {
    db_add_field('tmgmt_ds_server_client', 'ds_uid', $schema['tmgmt_ds_server_client']['fields']['ds_uid']);

    foreach ($schema['tmgmt_ds_server_client']['indexes'] as $name => $fields) {
      if (db_index_exists('tmgmt_ds_server_client', $name)) {
        db_drop_index('tmgmt_ds_server_client', $name);
      }
      db_add_index('tmgmt_ds_server_client', $name, $fields);
    }
  }
}

/**
 * Implements hook_install().
 */
function tmgmt_server_install() {
  // Create Client role and assign appropriate permissions to it.
  $remote_client = user_role_load_by_name('remote user');
  if (empty($remote_client)) {
    $remote_client = new stdClass();
    $remote_client->name = st('remote user');
    user_role_save($remote_client);
    variable_set('tmgmt_server_tc_rid', $remote_client->rid);
  }
  user_role_grant_permissions($remote_client->rid, array('use translation services'));
}

/**
 * Implements hook_uninstall().
 */
function tmgmt_auth_uninstall() {
  // Clean up the variables table.
  variable_del('tmgmt_server_client_limit');
}


/**
 * Implements hook_schema().
 */
function tmgmt_server_schema() {

  $schema['tmgmt_server_remote_source'] = array(
    'description' => 'Remote sources are loaded into the database every time a new translation job is ordered by a remote client.',
    'fields' => array(
      'rsid' => array(
        'description' => 'The identifier of the remote source.',
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'cid' => array(
        'description' => '{tmgmt_server_client}.cid',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'label' => array(
        'description' => 'Optional client-provided label for this remote source.',
        'type' => 'varchar',
        'length' => 255,
      ),
      'uid' => array(
        'description' => 'The user.uid that this remote source belongs to.',
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'source_language' => array(
        'description' => 'The source language of the data.',
        'type' => 'varchar',
        'length' => 12,
        'not null' => TRUE,
      ),
      'target_language' => array(
        'description' => 'The language the data should be translated to.',
        'type' => 'varchar',
        'length' => 12,
        'not null' => TRUE,
      ),
      'data' => array(
        'description' => 'The translation data.',
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'big',
        'serialize' => TRUE,
      ),
      'callback' => array(
        'description' => 'The callback URL that should be used to inform the remote client about the finished translation.',
        'type' => 'varchar',
        'length' => 255,
      ),
      'reference' => array(
        'description' => 'The client reference for the data to be translated.',
        'type' => 'varchar',
        'length' => 30,
      ),
      'ds_url' => array(
        'description' => 'TMGMT Directory url that mediated the job',
        'type' => 'varchar',
        'length' => 100,
      ),
      'state' => array(
        'description' => 'The state of the remote source.',
        'type' => 'int',
        'not null' => TRUE,
        // Defaults to the 'pending' state.
        'default' => 0,
      ),
      'user_agent' => array(
        'description' => 'Info about the client system sent via User-Agent in headers.',
        'type' => 'varchar',
        'length' => 255,
      ),
      'custom_data' => array(
        'description' => 'Custom data that can provide client to the server to extend the logic at the server.',
        'type' => 'varchar',
        'length' => 255,
      ),
      'changed' => array(
        'description' => 'Timestamp for when the remote source was most recently updated.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'created' => array(
        'description' => 'Created timestamp.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('rsid'),
    'foreign keys' => array(
      'user' => array(
        'table' => 'users',
        'columns' => array('uid' => 'uid'),
      ),
    ),
  );

  $schema['tmgmt_directory_server'] = array(
    'description' => 'TMGMT Directorys.',
    'fields' => array(
      'sid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Primary key.',
      ),
      'url' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'TMGMT Directory url.',
      ),
      'remote_identifier' => array(
        'description' => 'Remote identifier of the local system.',
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
    ),
    'primary key' => array('sid'),
    'indexes' => array(
      'url' => array('url'),
    ),
  );

  $schema['tmgmt_server_client'] = array(
    'description' => 'Access to the web server interface is solely granted through a client entity.',
    'fields' => array(
      'cid' => array(
        'description' => 'The identifier of the client entity.',
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => 'The label of the client entity.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => 'The description of the client entity.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'uuid' => array(
        'description' => 'Client UUID.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'uid' => array(
        'description' => 'The user that this client entity belongs to.',
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'ds_uid' => array(
        'description' => 'DS user identifier.',
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'changed' => array(
        'description' => 'Timestamp for when the client entity was most recently updated.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'created' => array(
        'description' => 'Created timestamp.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('cid'),
    'foreign keys' => array(
      'uid' => array(
        'table' => 'users',
        'columns' => array('uid'),
      ),
    ),
    'indexes' => array(
      'idx_uuid' => array('uuid'),
      'uid' => array('uid'),
      'ds_uid' => array('ds_uid'),
    ),
    'unique keys' => array(
      'unq_uuid' => array('uuid'),
    ),
  );

  $schema = array_merge($schema, tmgmt_auth_get_schema_info('issuer'));

  $schema = array_merge($schema, tmgmt_auth_get_schema_info('receiver'));

  return $schema;
}
